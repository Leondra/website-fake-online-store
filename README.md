This project was made for our intro to software engineering class. The goal was to create a website similar to etsy or ebay where users can upload their own products to sell. For this project I took the role as the scrum leader, I did most of the backend coding (such as uploading items and viewing them) and a bulk of the php code (login, admin access, etc.) and other styling with html and css. I also troubleshooted coding problems that the other team members struggled with. 

Group Name: Fake Business 
Date: Fall 2021
Participants: Elizabeth, Brian, Isaiah
